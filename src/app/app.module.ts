import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabase } from '@angular/fire/database';

import { AppRoutingModule } from './app-routing.module';
import { environment } from '../environments/environment';

// Components
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ListsComponent } from './components/lists/lists.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { JobComponent } from './components/job/job.component';

// Services
import { DataService } from './services/data.service';

// Guards
import { AuthGuard } from './guards/auth.guard';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ListsComponent,
    DashboardComponent,
    JobComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseAdmin, 'firebaseAdmin'),
    AngularFireModule.initializeApp(environment.firebaseBehired, 'firebaseBehired'),
    AngularFireAuthModule,
    BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(AppRoutingModule)
  ],
  providers: [
    DataService,
    AuthGuard,
    AngularFireDatabase
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
