import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { DataService } from '../services/data.service';
import { take, map } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private dataService: DataService,
    private router: Router
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    return this.dataService.getUser()
      .pipe(
        take(1),
        map(user => {
          const whiteList = [ 'hello@behired.co.uk', 'markoddi1387@gmail.com', 'chris@behired.co.uk' ];
          if (user && whiteList.includes(user.email)) {
            return true;
          } else {
            this.dataService.logout()
              .pipe(
                take(1)
              ).subscribe(() => this.router.navigate(['/login']));
          }
        })
      );
  }
}


