import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';

import { environment } from '../../environments/environment';

@Injectable()
export class DataService {
  public app;

  constructor(
    public angularFireDatabase: AngularFireDatabase,
    private angularFireAuth: AngularFireAuth,
    private angularFirestore: AngularFirestore
  ) {
    this.app = firebase.initializeApp(environment.firebaseBehired);
  }

  public login(email: string, password: string): Observable<firebase.auth.UserCredential> {
    const login = this.angularFireAuth.signInWithEmailAndPassword(email, password);
    return from(login.then(response => response).catch(e => e));
  }

  public getUsers(tableName: string): Observable<any> {
    const ref = this.app.database().ref(`${ tableName }`);
    const listObs = this.angularFireDatabase.list(ref);

    return from(listObs.snapshotChanges()
      .pipe(
        map(changes => {
          return changes.map(item => item.payload.val());
        })
      ));
  }

  public getJobs(): Observable<any> {
    return this.angularFirestore.collection('behired').stateChanges()
      .pipe(
        map(changes => {

          return changes.map(item => {
            const doc = item.payload.doc;
            const row = doc.data()['d'];
            const id = item.payload.doc.id;

            return {
              address: row.address.addressLine1,
              name: row.name,
              isActive: row.isActive,
              key: id,
              sector: row.sector,
              pay: `£ ${row.pay}`
            };
          });
        })
      );
  }

  public logout(): Observable<any> {
    return from(this.angularFireAuth.signOut());
  }

  public getUser(): Observable<firebase.User> {
    return this.angularFireAuth.authState;
  }

  public getJob(jobId: string): Observable<any> {
    return this.angularFirestore.collection('behired').doc(jobId).valueChanges()
      .pipe(
        map(changes => {
          return changes['d'];
        })
      );
  }

  public getCompany(employerId: string): any {
    return this.angularFireDatabase.object('employers/' + employerId).snapshotChanges().pipe(
      map(res => res.payload.val())
    );
  }

  public getLastThreeJobs(): Observable<any> {
    const ref = firebase.firestore().collection('behired').orderBy('d.datePosted', 'desc').limit(3);
    return from(ref.get().then(data => {
      let jobs = [];

      data.forEach((doc) => {
        const job = doc.data().d || doc.data();

        const newJob = {
          address: job.address.addressLine1,
          name: job.name,
          isActive: job.isActive,
          key: doc.id,
          sector: job.sector,
          pay: `£ ${job.pay}`
        };

        jobs = [ ...jobs, newJob ];
      });
      return jobs;
    }));
  }
}
