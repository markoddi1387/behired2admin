import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';

import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss']
})
export class ListsComponent implements OnInit, OnDestroy {
  public tableOptions = [ 'jobs', 'applicants', 'employers' ];
  public selectedTable: any;
  public destroyed$: Subject<boolean> = new Subject<boolean>();
  public form: FormGroup;
  public currentList = 'jobs';
  public isNoteVisible = true;
  public lastThreeJobs: any;

  constructor(
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.routeListener();
    this.createForm();
    this.formListener();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(false);
    this.destroyed$.complete();
  }

  public routeListener(): void {
    this.activatedRoute.params.subscribe(params => {
      const type = params['id'];

      if (type) {
        this.getTableData(type);
      } else {
        this.getTableData('jobs');
      }
    });
  }

  public formListener(): void {
    this.form.controls['table'].valueChanges
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((selectedValue) => this.getTableData(selectedValue));
  }

  public createForm(): void {
    this.form = this.formBuilder.group({
      table: [, [ ]]
    });
  }

  public getTableData(list: string): void {
    this.currentList = list;

    if (list !== 'jobs') {
      this.dataService.getUsers(list)
        .pipe(
          takeUntil(this.destroyed$)
        ).subscribe((data) => {
          this.selectedTable = data;
        });
    } else {
      this.dataService.getJobs()
        .pipe(
          takeUntil(this.destroyed$)
        ).subscribe((data) => this.selectedTable = data);
    }

    this.getLastThreeJobs();
  }

  public getLastThreeJobs(): void {
    this.dataService.getLastThreeJobs()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((jobs) => {
        this.lastThreeJobs = jobs;
      });
  }

  public logout(): void {
    this.dataService.logout()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => this.router.navigate(['/login']));
  }
}
