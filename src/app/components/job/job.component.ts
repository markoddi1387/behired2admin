import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.scss']
})
export class JobComponent implements OnInit, OnDestroy {
  public destroyed$: Subject<boolean> = new Subject<boolean>();
  public job: any;
  public employer: any;
  public isDescriptionVisible = false;

  constructor(
    private dataService: DataService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.routeListener();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(false);
    this.destroyed$.complete();
  }

  public routeListener(): void {
    this.activatedRoute.params.subscribe(params => {
      const key = params['id'];
      this.getJob(key);
    });
  }

  public getJob(key: string): void {
    this.dataService.getJob(key)
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((job: any) => {
        this.job = job;
        this.getCompany(job.employerId);
      });
  }

  public getCompany(employerId: string): void {
    this.dataService.getCompany(employerId)
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe((employer: any) => {
        this.employer = employer;
      });
  }

  public logout(): void {
    this.dataService.logout()
      .pipe(
        takeUntil(this.destroyed$)
      ).subscribe(() => this.router.navigate(['/login']));
  }
}
