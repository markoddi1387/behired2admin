import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { take } from 'rxjs/operators';
import { Router } from '@angular/router';

import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public form: FormGroup;

  constructor(
    private dataService: DataService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  public createForm(): void {
    this.form = this.formBuilder.group({
      email: [, [ Validators.required ]],
      password: [, [ Validators.required ]]
    });
  }

  public onSubmit(): void {
    const { email, password } = this.form.value;

    if (this.form.valid) {
      this.dataService.login(email, password)
        .pipe(
          take(1)
        ).subscribe(data => {
          if (data.user) {
            this.router.navigate(['/dashboard']);
          }
        });
    }
  }
}
