import { Routes } from '@angular/router';

// Components:
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ListsComponent } from './components/lists/lists.component';
import { JobComponent } from './components/job/job.component';

// Guards:
import { AuthGuard } from './guards/auth.guard';

export const AppRoutingModule: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'job/:id',
    component: JobComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'lists',
    component: ListsComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'lists/:id',
    component: ListsComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: '**', redirectTo: 'login'
  }
];
