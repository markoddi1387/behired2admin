export const environment = {
  production: true,
  firebaseAdmin: {
    apiKey: 'AIzaSyCDDQOREn4qEDa67Tcq9oI6n1mMUjawWag',
    authDomain: 'behired-admin.firebaseapp.com',
    databaseURL: 'https://behired-admin.firebaseio.com',
    projectId: 'behired-admin',
    storageBucket: 'behired-admin.appspot.com',
    messagingSenderId: '273190168720'
  },
  firebaseBehired: {
    apiKey: 'AIzaSyAw3gV-BYM3uClnHKoQgEEBW03lGKMX7oo',
    authDomain: 'leisure-project.firebaseapp.com',
    databaseURL: 'https://leisure-project.firebaseio.com',
    projectId: 'leisure-project',
    storageBucket: 'leisure-project.appspot.com',
    messagingSenderId: '316646279871'
  }
};
